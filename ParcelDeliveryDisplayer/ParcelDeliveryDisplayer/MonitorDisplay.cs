﻿using System;
using ParcelDeliveryDisplayer.Objects;
using ParcelDeliveryDisplayer.BusinessLogicLayer;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParcelDeliveryDisplayer
{
    public partial class MonitorDisplay : Form
    {
        private List<ParcelTransaction> parcelTrans;
        private parcelTransactionLogic parcelTransLogicLayer;
        public MonitorDisplay()
        {
            this.parcelTransLogicLayer = new parcelTransactionLogic();
            InitializeComponent();

            this.BindDataSourceToGrid();
            this.HideSpecificColumns();
            this.SetColumnDisplayIndex();
        }

        private void SetColumnDisplayIndex()
        {
            this.dataGridViewMonitor.Columns["Barcode"].DisplayIndex = 0;
            this.dataGridViewMonitor.Columns["DateReceived"].DisplayIndex = 1;
            this.dataGridViewMonitor.Columns["ParcelStatus"].DisplayIndex = 2;
            this.dataGridViewMonitor.Columns["StudentName"].DisplayIndex = 3;
            this.dataGridViewMonitor.Columns["StudentMatricNo"].DisplayIndex = 4;
            this.dataGridViewMonitor.Columns["HpNo"].DisplayIndex = 5;

        }

        private void HideSpecificColumns()
        {
            this.dataGridViewMonitor.Columns["Id"].Visible = false;
            this.dataGridViewMonitor.Columns["StudentId"].Visible = false;
            this.dataGridViewMonitor.Columns["ParcelId"].Visible = false;
            this.dataGridViewMonitor.Columns["UserId"].Visible = false;
            this.dataGridViewMonitor.Columns["ReceivedBy"].Visible = false;
            this.dataGridViewMonitor.Columns["DateCollected"].Visible = false;
            this.dataGridViewMonitor.Columns["DateReturned"].Visible = false;
            this.dataGridViewMonitor.Columns["ParcelStatus"].Visible = false;

        }

        private void BindDataSourceToGrid()
        {
            this.dataGridViewMonitor.DataSource = this.parcelTransLogicLayer.GetParcelTransactionDataSourceForMonitor();
            this.dataGridViewMonitor.ClearSelection();
        }

        private void dataGridViewMonitor_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            using (SolidBrush b = new SolidBrush(dataGridViewMonitor.RowHeadersDefaultCellStyle.ForeColor))
            {
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, b, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 5);
            }
        }
    }
}
