﻿using ParcelDeliveryDisplayer.Objects;
using ParcelDeliveryDisplayer.BusinessLogicLayer;
using System;
using System.Collections.Generic;

using System.Windows.Forms;

namespace ParcelDeliveryDisplayer
{
    public partial class EditParcel : Form
    {
        private ParcelTransaction parcelTrans;
        private Student student;
        private Parcel parcel;

        private ParcelLogic parcelLogicLayer;
        private StudentLogic studentLogicLayer;
        public EditParcel(ParcelTransaction selectedParcelTrans)
        {
            this.parcelTrans = selectedParcelTrans;
            this.parcelLogicLayer = new ParcelLogic();
            this.studentLogicLayer = new StudentLogic();
            //get the parcel and student object
            this.parcel = this.parcelLogicLayer.GetParcelByParcelId(this.parcelTrans.ParcelId);
            this.student = this.studentLogicLayer.GetStudentByStudentId(this.parcelTrans.StudentId);

            InitializeComponent();

            BindParcelStatusComboBoxData();
            PopulateFields();
            ToogleButtonsEnability(parcelTrans);
        }

        private void ToogleButtonsEnability(ParcelTransaction parcelTrans)
        {
            if(parcelTrans.ParcelStatus == "Returned")
            {
                MessageBox.Show("Unable to edit parcel details, This parcel have been returned");
                this.buttonSave.Enabled = false;
                this.comboBoxParcelStatus.Enabled = false;
                this.textBoxStudentName.Enabled = false;
                this.textBoxStudentMatricNo.Enabled = false;
                this.textBoxBarcode.Enabled = false;
                this.textBoxHpNo.Enabled = false;
            }
            if (parcelTrans.ParcelStatus == "Collected")
            {
                MessageBox.Show("Unable to edit parcel details, This parcel have been Collected");
                this.buttonSave.Enabled = false;
                this.comboBoxParcelStatus.Enabled = false;
                this.textBoxStudentName.Enabled = false;
                this.textBoxStudentMatricNo.Enabled = false;
                this.textBoxBarcode.Enabled = false;
                this.textBoxHpNo.Enabled = false;
            }
        }

        private void PopulateFields()
        {
            this.textBoxParcelId.Text = this.parcelTrans.ParcelId;
            this.textBoxBarcode.Text = this.parcelTrans.Barcode;
            this.textBoxStudentName.Text = this.parcelTrans.StudentName;
            this.textBoxStudentMatricNo.Text = this.parcelTrans.StudentMatricNo;
            this.textBoxHpNo.Text = this.parcelTrans.HpNo;
            if(this.parcelTrans.ParcelStatus == "New")
            {
                this.labelDate.Text = "Date Received";
                this.dateTimePickerDate.Value = this.parcelTrans.DateReceived;
            }
            else if(this.parcelTrans.ParcelStatus == "Collected")
            {
                this.labelDate.Text = "Date Collected";
                this.dateTimePickerDate.Value = this.parcelTrans.DateCollected;
            }
            else
            {
                this.labelDate.Text = "Date Returned";
                this.dateTimePickerDate.Value = this.parcelTrans.DateReturned;
            }
            this.comboBoxParcelStatus.SelectedIndex = GetParcelStatusSelectedIndex(this.parcelTrans.ParcelStatus);
        }

        private int GetParcelStatusSelectedIndex(string parcelStatus)
        {
            if (parcelStatus == "New") return 0;
            if (parcelStatus == "Collected") return 1;
            else return 2;
        }

        private void BindParcelStatusComboBoxData()
        {
            this.comboBoxParcelStatus.DataSource = ParcelStatuses();
        }
        private List<string> ParcelStatuses()
        {
            List<string> statuses = new List<string>();
            statuses.Add("New");
            statuses.Add("Collected");
            statuses.Add("Returned");
            return statuses;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if(ValidateFields() == false)
            {
                return;
            }

            //apply changes to object
            this.parcel.Barcode = this.textBoxBarcode.Text;

            this.student.Name = this.textBoxStudentName.Text;
            this.student.MatricNo = this.textBoxStudentMatricNo.Text;
            this.student.HpNo = this.textBoxHpNo.Text;

            this.parcelTrans.ParcelStatus = this.comboBoxParcelStatus.SelectedValue.ToString();

            if(this.parcelLogicLayer.UpdateParcel(parcel, student, parcelTrans) == true)
            {
                MessageBox.Show("Parcel details changes saved!");
                this.Close();
            }

           
        }

        private bool ValidateFields()
        {
            bool result = true;

            if(String.IsNullOrEmpty(this.textBoxBarcode.Text))
            {
                MessageBox.Show("Barcode is required!. Please rescan item");
                result = false;
            }
            if (String.IsNullOrEmpty(this.textBoxStudentName.Text))
            {
                MessageBox.Show("Student name is required!");
                result = false;

            }
            if (String.IsNullOrEmpty(this.textBoxHpNo.Text))
            {
                MessageBox.Show("Hp No is required!. Please enter hp no as stated on parcel");
                result = false;

            }

            if (this.comboBoxParcelStatus.SelectedIndex == 2)
            {
                DialogResult warning = MessageBox.Show("Setting parcel as 'Returned' will remove this item from monitor list, proceed?", "Caution", MessageBoxButtons.OKCancel);
                switch (warning)
                {
                    case DialogResult.OK:
                        {
                            result = true;
                            break;
                        }
                    case DialogResult.Cancel:
                        {
                            result = false;
                            break;
                        }
                }
            }
            else
            {
                result = true;
            }

            return result;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
