﻿using System;
using ParcelDeliveryDisplayer.BusinessLogicLayer;
using ParcelDeliveryDisplayer.Objects;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ParcelDeliveryDisplayer.DataAccessLayer;

namespace ParcelDeliveryDisplayer
{
    public partial class Settings : Form
    {
        private LoginData loginData;
        private UserLogic userLogicLayer;
        public Settings()
        {
            InitializeComponent();

            this.loginData = new LoginData();
            this.userLogicLayer = new UserLogic();

            this.BindGridToDataSource();
        }

        private void BindGridToDataSource()
        {
            this.dataGridViewUsers.DataSource = this.userLogicLayer.GetAllUsers();
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            this.ShowTabPage("tabPageConnection");
            DbConnection.StartDbConnection();
            if (DbConnection.Connection != null)
            {
                this.labelResult.Text = "Successful!";
                this.labelResult.BackColor = Color.Green;
            }
            else
            {
                this.labelResult.Text = "Connection Failed!";
                this.labelResult.BackColor = Color.Red;
            }
        }

        private void ShowTabPage(string tabpageName)
        {
            foreach(TabPage tab in tabControlSetting.TabPages)
            {
                if(tab.Name == tabpageName)
                {
                    tabControlSetting.SelectedTab = tab;
                    return;
                }
            }
        }

        private void buttonHistory_Click(object sender, EventArgs e)
        {
            this.ShowTabPage("tabPageLogin");

            //this.dataGridViewLoginHistory.Columns[0].Name = "No";
            this.dataGridViewUsers.DataSource = this.loginData.GetLoginTransactions();


            this.dataGridViewUsers.Columns["UserId"].HeaderText = "Staff Name";

            this.dataGridViewUsers.Columns["LoginDate"].HeaderText = "Login Date";

            this.dataGridViewUsers.Columns["LoginTime"].HeaderText = "Login Time";

            this.dataGridViewUsers.Columns["LogoutTime"].Name = "Logout Time";

            this.dataGridViewUsers.DataSource = this.loginData.GetLoginTransactions();
        }

        private void dataGridViewLoginHistory_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            using (SolidBrush b = new SolidBrush(dataGridViewUsers.RowHeadersDefaultCellStyle.ForeColor))
            {
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, b, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 4);
            }
        }

        private void buttonAddStaff_Click(object sender, EventArgs e)
        {
            NewUser newUserForm = new NewUser();
            newUserForm.FormClosed += NewUserForm_FormClosed;
            newUserForm.Show();
        }

        private void NewUserForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.RefreshData();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDeleteUser_Click(object sender, EventArgs e)
        {
            bool proceed = false;
            User selectedUser = this.dataGridViewUsers.CurrentRow.DataBoundItem as User;

            if(selectedUser.Role == "admin")
            {
                MessageBox.Show("Not allowed to delete Admin account!");
                return;
            }

            DialogResult result = MessageBox.Show("Deleted data will not be retrieveable, confirm to delete " + selectedUser.UserId + " ?", "Warning", MessageBoxButtons.OKCancel);

            switch (result)
            {
                case DialogResult.OK:
                    {
                        //proceed delete
                        proceed = true;
                        break;
                    }
                case DialogResult.Cancel:
                    {
                        //cancel
                        proceed = false;
                        break;
                    }
            }

            if (proceed)
            {
                //delete
                if (this.userLogicLayer.DeleteUserByUserId(selectedUser.UserId) == true)
                {
                    MessageBox.Show("Data have been deleted!");
                    this.RefreshData();
                }
            }
            else
            {
                return;
            }
        }

        private void RefreshData()
        {
            this.BindGridToDataSource();

        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            this.RefreshData();
        }
    }
}
