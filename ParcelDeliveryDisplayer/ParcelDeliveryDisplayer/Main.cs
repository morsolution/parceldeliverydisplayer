﻿using ParcelDeliveryDisplayer.Objects;
using ParcelDeliveryDisplayer.BusinessLogicLayer;
using ParcelDeliveryDisplayer.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParcelDeliveryDisplayer
{
    public partial class Main : Form
    {
        private List<ParcelTransaction> transactions = new List<ParcelTransaction>();
        private parcelTransactionLogic parcelTransactionLogic;
        private UserLogic userLogicLayer;
        private User currentUser;
        public Main()
        {
            InitializeComponent();

            this.userLogicLayer = new UserLogic();
            this.parcelTransactionLogic = new parcelTransactionLogic();

            this.currentUser = this.userLogicLayer.GetCurrentUser(Global.CurrentUserUsername);
            this.labelAdmin.Text = this.currentUser.Name;

            this.BindDataSourceToGrid();
            this.BindFilterDataSource();
            this.HideSpecificColumns();
            this.SetColumnDisplayIndex();
            this.CheckUserRole();
        }

        private void BindFilterDataSource()
        {
            this.comboBoxFilter.DataSource = Filters();
        }

        private List<string> Filters()
        {
            List<string> statuses = new List<string>();
            statuses.Add("Name");
            statuses.Add("Barcode");
            statuses.Add("HP No");
            return statuses;
        }

        private void CheckUserRole()
        {
            if(UserSession.getCurrentUser().Role != "admin")
            {
                this.buttonSettings.Visible = false;
            }
            else
            {
                this.buttonSettings.Visible = true;
            }
        }

        private void SetColumnDisplayIndex()
        {
            this.dataGridViewTransactions.Columns["ParcelId"].DisplayIndex = 0;
            this.dataGridViewTransactions.Columns["Barcode"].DisplayIndex = 1;
            this.dataGridViewTransactions.Columns["ParcelStatus"].DisplayIndex = 2;
            this.dataGridViewTransactions.Columns["StudentName"].DisplayIndex = 3;
            this.dataGridViewTransactions.Columns["StudentMatricNo"].DisplayIndex =4;
            this.dataGridViewTransactions.Columns["HpNo"].DisplayIndex = 5;
            this.dataGridViewTransactions.Columns["DateReceived"].DisplayIndex = 6;
            this.dataGridViewTransactions.Columns["DateCollected"].DisplayIndex = 7;
            this.dataGridViewTransactions.Columns["DateReturned"].DisplayIndex = 8;
        }

        private void HideSpecificColumns()
        {
            this.dataGridViewTransactions.Columns["Id"].Visible = false;
            this.dataGridViewTransactions.Columns["StudentId"].Visible = false;
            this.dataGridViewTransactions.Columns["UserId"].Visible = false;
        }

        private void buttonNewItem_Click(object sender, EventArgs e)
        {
            AddParcel addParcelForm = new AddParcel();
            addParcelForm.FormClosed += AddParcelForm_FormClosed;
            addParcelForm.Show();
        }

        private void AddParcelForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            RefreshData();
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            Settings settingsForm = new Settings();
            settingsForm.Show();
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            Login loginForm = new Login();
            loginForm.Show();
            this.Hide();
        }

        private void BindDataSourceToGrid()
        {
            //get all from parceltransaction

            //bind to datasource
            this.dataGridViewTransactions.DataSource = this.parcelTransactionLogic.GetParcelTransactionDataSource();

            //if no data, edit,delete,collection should not be enabled
            if (this.parcelTransactionLogic.GetParcelTransactionDataSource().Count == 0) //if no data
            {
                SetButtonsEnability(false);
            }
            else
            {
                SetButtonsEnability(true);
            }
        }

        private void SetButtonsEnability(bool condition)
        {
            if(condition == true)
            {
                this.buttonEdit.Enabled = true;
                this.buttonDelete.Enabled = true;
                this.buttonCollect.Enabled = true;
            }
            else
            {
                this.buttonEdit.Enabled = false;
                this.buttonDelete.Enabled = false;
                this.buttonCollect.Enabled = false;
            }
        }

        private void SetColumnNames()
        {
            this.dataGridViewTransactions.Columns["ParcelId"].HeaderText = "Parcel ID";
            this.dataGridViewTransactions.Columns["Barcode"].HeaderText = "Parcel Barcode";
            this.dataGridViewTransactions.Columns["StudentName"].HeaderText = "Student";
            this.dataGridViewTransactions.Columns["StudentMatricNo"].HeaderText = "Matric No";
            this.dataGridViewTransactions.Columns["ReceivedBy"].HeaderText = "Received By";
            this.dataGridViewTransactions.Columns["ParcelStatus"].HeaderText = "Parcel Status";
            this.dataGridViewTransactions.Columns["DateReceived"].HeaderText = "Received Date";
            this.dataGridViewTransactions.Columns["DateCollected"].HeaderText = "Collected Date";

        }

        private void dataGridViewTransactions_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            using (SolidBrush b = new SolidBrush(dataGridViewTransactions.RowHeadersDefaultCellStyle.ForeColor))
            {
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, b, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 4);
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            //get selected parcel from parcel grid
            ParcelTransaction selectedParcel = this.dataGridViewTransactions.CurrentRow.DataBoundItem as ParcelTransaction;

            //if no parcel selected, complain
            if(selectedParcel == null)
            {
                MessageBox.Show("Please Select a record");
                return;
            }

            EditParcel editParcel = new EditParcel(selectedParcel);
            editParcel.FormClosed += EditParcel_FormClosed;
            editParcel.Show();
        }

        private void EditParcel_FormClosed(object sender, FormClosedEventArgs e)
        {
            RefreshData();
        }

        private void dataGridViewTransactions_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach(DataGridViewRow row in dataGridViewTransactions.Rows)
            {
                if (row.Cells["ParcelStatus"].Value.ToString() == "New")
                {
                    row.Cells["ParcelStatus"].Style.BackColor = Color.LightGreen;
                }

                if (row.Cells["ParcelStatus"].Value.ToString() == "Collected")
                {
                    row.Cells["ParcelStatus"].Style.BackColor = Color.LightBlue;
                }

                if (row.Cells["ParcelStatus"].Value.ToString() == "Returned")
                {
                    row.Cells["ParcelStatus"].Style.BackColor = Color.MediumVioletRed;
                }
            }
        }

        private void RefreshData()
        {
            BindDataSourceToGrid();
            this.HideSpecificColumns();
            this.SetColumnDisplayIndex();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void buttonCollect_Click(object sender, EventArgs e)
        {
            ParcelTransaction selectedParcel = this.dataGridViewTransactions.CurrentRow.DataBoundItem as ParcelTransaction;
            CollectParcel collectParcel = new CollectParcel(selectedParcel);
            collectParcel.FormClosed += CollectParcel_FormClosed;
            collectParcel.Show();
        }

        private void CollectParcel_FormClosed(object sender, FormClosedEventArgs e)
        {
            RefreshData();
        }

        private void buttonDisplay_Click(object sender, EventArgs e)
        {
            MonitorDisplay monitor = new MonitorDisplay();
            monitor.StartPosition = FormStartPosition.Manual;
            monitor.Show();

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            bool proceed = false;
            ParcelTransaction selectedParcel = this.dataGridViewTransactions.CurrentRow.DataBoundItem as ParcelTransaction;

            DialogResult result = MessageBox.Show("Deleted data will not be retrieveable, confirm to delete " + selectedParcel.ParcelId + " ?", "Warning", MessageBoxButtons.OKCancel);

            switch (result)
            {
                case DialogResult.OK:
                    {
                        //proceed delete
                        proceed = true;
                        break;
                    }
                case DialogResult.Cancel:
                    {
                        //cancel
                        proceed = false;
                        break;
                    }
            }

            if(proceed)
            {
                //delete
                if(this.parcelTransactionLogic.DeleteParcelTransaction(selectedParcel.Id) == true)
                {
                    MessageBox.Show("Data have been deleted!");
                }
            }
            else
            {
                return;
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(this.textBoxSearch.Text))
            {
                this.BindDataSourceToGrid();
            }
            else
            {
                string filterType = this.comboBoxFilter.SelectedValue.ToString();
                string filter = this.textBoxSearch.Text;

                this.dataGridViewTransactions.DataSource = this.parcelTransactionLogic.GetAllParcelTransactionsByFilter(filterType, filter);
            }

        }
    }
}
