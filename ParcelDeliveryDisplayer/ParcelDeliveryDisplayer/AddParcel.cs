﻿using ParcelDeliveryDisplayer.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using ParcelDeliveryDisplayer.BusinessLogicLayer;
using System.Windows.Forms;

namespace ParcelDeliveryDisplayer
{
    public partial class AddParcel : Form
    {
        private ParcelLogic parcelLogicLayer;
        public AddParcel()
        {
            InitializeComponent();
            this.parcelLogicLayer = new ParcelLogic();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if(ValidateFields() == false)
            {
                return;
            }

            Parcel parcel = new Parcel();
            parcel.Barcode = this.textBoxBarcode.Text;
            parcel.DateReceived = this.dateTimePickerDateReceived.Value;

            Student student = new Student();
            student.Name = this.textBoxStudentName.Text;
            student.HpNo = this.textBoxHpNo.Text;

            if(this.parcelLogicLayer.AddParcel(parcel,student)) //if AddParcel is succesfull it should return true
            {
                MessageBox.Show("Parcel have been added");
                this.Close();
            }

        }

        private bool ValidateFields()
        {
            bool result = true;

            if(String.IsNullOrEmpty(this.textBoxBarcode.Text))
            {
                MessageBox.Show("Please scan the barcode!");
                result = false;
            }

            if(String.IsNullOrEmpty(this.textBoxStudentName.Text))
            {
                MessageBox.Show("Student name is required");
                result = false;

            }

            if (this.parcelLogicLayer.VerifyParcelExists(this.textBoxBarcode.Text))
            {
                MessageBox.Show("Parcel has already been added, please add a new parcel");
                result = false;
            }

            return result;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddParcel_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Please Scan the barcode on the parcel.");
        }

    }
}
