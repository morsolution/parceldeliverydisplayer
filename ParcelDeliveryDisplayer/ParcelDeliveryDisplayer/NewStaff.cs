﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ParcelDeliveryDisplayer.DataAccessLayer;
using ParcelDeliveryDisplayer.Objects;

namespace ParcelDeliveryDisplayer
{
    public partial class NewUser : Form
    {
        public NewUser()
        {
            InitializeComponent();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            UserData userDataLayer = new UserData();
            User user = new User();

            if(validateFields() == false)
            {
                MessageBox.Show("Please Enter All Details!");
                return;
            }

            user.Name = this.textBoxName.Text;
            user.UserId = this.textBoxStaffId.Text;
            user.Username = this.textBoxUsername.Text;
            user.Password = this.textBoxPassword.Text;
            user.Role = "staff";


            if (userDataLayer.InsertUser(user) == true)
            {
                MessageBox.Show("User Added Successfuly!");
                IdAutoNumberData.UpdateNextUserIdAutoNumber();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Failed to add new user!");
            }
        }

        private bool validateFields()
        {
            bool result = true;

            if(string.IsNullOrEmpty(textBoxName.Text))
            {
                result = false;
            }

            if (string.IsNullOrEmpty(textBoxStaffId.Text))
            {
                result = false;
            }
        
            if (string.IsNullOrEmpty(textBoxUsername.Text))
            {
                result = false;
            }

            if (string.IsNullOrEmpty(textBoxPassword.Text))
            {
                result = false;
            }

            return result;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void NewUser_Load(object sender, EventArgs e)
        {
            this.textBoxStaffId.Text = IdAutoNumberData.GetNextUserId();

        }
    }
}
