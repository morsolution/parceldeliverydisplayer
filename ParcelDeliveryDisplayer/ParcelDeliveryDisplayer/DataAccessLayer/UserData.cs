﻿using ParcelDeliveryDisplayer.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace ParcelDeliveryDisplayer.DataAccessLayer
{
    public class UserData
    {
        public UserData()
        {
          
        }

        public User RetrieveUserByUsername(string username)
        {
            User user = new User();
            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            if(DbConnection.Connection != null)
            {
                MySqlCommand command = DbConnection.Connection.CreateCommand();
                command.CommandText = "SELECT * FROM user where username = @username";
                command.Parameters.AddWithValue("@username", username);

                try
                {
                    MySqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        user.Id = reader.GetInt32("id");
                        user.Name = reader.GetString("Name");
                        user.UserId = reader.GetString("userId");
                        user.Username = reader.GetString("username");
                        user.Password = reader.GetString("password");
                        user.Role = reader.GetString("role");
                    }

                    reader.Close();
                }
                catch(Exception ex)
                {

                }

            }
            return user;
        }

        public List<User> RetrieveAllUsers()
        {
            List<User> users = new List<User>();
            DataTable dataTable = new DataTable();

            //join table parcel,student,user and parcel_transaction
            string query = "SELECT * FROM user";

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }
            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);

            using (MySqlDataAdapter adapter = new MySqlDataAdapter(comm))
            {
                adapter.Fill(dataTable);
            }

            foreach (DataRow row in dataTable.Rows)
            {
                User user = new User();
                user.Id = (int)row["id"];
                user.UserId = row["userId"].ToString();
                user.Username = row["username"].ToString();
                user.Role = row["role"].ToString();
                user.Password = row["password"].ToString();
                user.Name = row["name"].ToString();

                users.Add(user);
            }

            return users;
        }

        public bool InsertUser(User user)
        {
            bool result = false;
            string query = "INSERT INTO user(name,userId,username,password,role) VALUES(@name, @userId, @username, @password, @role)";

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            if (DbConnection.Connection != null)
            {
                MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
                comm.Parameters.AddWithValue("@name", user.Name);
                comm.Parameters.AddWithValue("@userId", IdAutoNumberData.GetNextUserId());
                comm.Parameters.AddWithValue("@username", user.Username);
                comm.Parameters.AddWithValue("@password", user.Password);
                comm.Parameters.AddWithValue("@role", user.Role);

                try
                {
                    comm.ExecuteNonQuery();
                    result = true;
                }
                catch(Exception ex)
                {

                }

                if(DbConnection.Connection != null)
                {
                    if(DbConnection.Connection.State == ConnectionState.Open)
                    {
                        DbConnection.CloseDbConnection();

                    }
                }
            }

            return result;

        }

        public void DeleteUserByUserId(string userId)
        {
            string query = "DELETE FROM user WHERE userId = @userId";
            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.Parameters.AddWithValue("@userId", userId);

            comm.ExecuteNonQuery();
            DbConnection.CloseDbConnection();
        }
    }
}
