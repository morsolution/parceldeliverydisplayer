﻿using ParcelDeliveryDisplayer.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelDeliveryDisplayer.DataAccessLayer
{
    public class LoginData
    {
        public LoginData()
        {
        }

        public object DbConnection { get; private set; }

        public List<LoginTransactions> GetLoginTransactions()
        {
            List<LoginTransactions> dummylist = new List<LoginTransactions>();
            LoginTransactions dummy1 = new LoginTransactions() { UserId = 1, LoginDate = DateTime.Now, LoginTime = DateTime.Now.TimeOfDay, LogoutTime = DateTime.Now.TimeOfDay };
            LoginTransactions dummy2 = new LoginTransactions() { UserId = 1, LoginDate = DateTime.Now, LoginTime = DateTime.Now.TimeOfDay, LogoutTime = DateTime.Now.TimeOfDay };

            dummylist.Add(dummy1);
            dummylist.Add(dummy2);

            return dummylist;

        }
    }
}
