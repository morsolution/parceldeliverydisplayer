﻿using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace ParcelDeliveryDisplayer.DataAccessLayer
{
    //static class so that it can be accessed anywhere in this project, and only be initialized once; when the project starts
    public static class DbConnection
    {
        private static string server;
        private static string database;
        private static string username;
        private static string password;
        private static MySqlConnection _connection;

        public static MySqlConnection Connection
        {
            get
            {
                return _connection;
            }
            set
            {
                _connection = value;
            }

        }

        public static bool HasDatabase
        {
            get
            {
                if(_connection != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void RestartDbConnection()
        {
            StopDbConnection();
            StartDbConnection();
        }
        public static void StartDbConnection()
        {
            server = "localhost";
            // change this to ur database name, for example imy database is parcel_delivery_displayer_01
            database = "parcel_delivery_displayer_01";
            username = "root";
            password = "123456";

            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + username + ";" + "PASSWORD=" + password + ";";

            _connection = new MySqlConnection(connectionString);

            try
            {
                _connection.Open();
            }
            catch(MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
            }
        }

        public static void StopDbConnection()
        {
            _connection = null;
        }

        public static void CloseDbConnection()
        {
            _connection.Close();
        }
    }
}
