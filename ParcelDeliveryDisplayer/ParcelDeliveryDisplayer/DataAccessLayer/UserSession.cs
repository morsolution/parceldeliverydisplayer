﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParcelDeliveryDisplayer.Objects;

namespace ParcelDeliveryDisplayer.DataAccessLayer
{
    public static class UserSession
    {
        private static User user;
        public static void SetUserSession(User currentUser)
        {
            user = currentUser;
        }
        public static User getCurrentUser()
        {
            return user;
        }
    }
}
