﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ParcelDeliveryDisplayer.DataAccessLayer
{
    public static class IdAutoNumberData
    {
        public static string GetNextParcelId()
        {
            int currentAutoNumber = 0;
            string nextParcelId = string.Empty;

            if(DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            string query = "SELECT * from id_autonumber WHERE prefix = 'PAR'";

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            MySqlDataReader reader = comm.ExecuteReader();

            while(reader.Read())
            {
                currentAutoNumber = reader.GetInt32("currentNumber");
            }

            nextParcelId = "PAR" +  (currentAutoNumber + 1);
            reader.Close();
            DbConnection.StopDbConnection();
            return nextParcelId;
        }

        public static string GetNextUserId()
        {
            int currentAutoNumber = 0;
            string nextParcelId = string.Empty;

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }
            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            string query = "SELECT * from id_autonumber WHERE prefix = 'USR'";

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            MySqlDataReader reader = comm.ExecuteReader();

            while (reader.Read())
            {
                currentAutoNumber = reader.GetInt32("currentNumber");
            }

            nextParcelId = "USR" + (currentAutoNumber + 1);
            reader.Close();
            DbConnection.StopDbConnection();
        
            return nextParcelId;
        }

        public static string GetNextStudentId()
        {
            int currentAutoNumber = 0;
            string nextParcelId = string.Empty;

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            string query = "SELECT * from id_autonumber WHERE prefix = 'STU'";

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            MySqlDataReader reader = comm.ExecuteReader();

            while (reader.Read())
            {
                currentAutoNumber = reader.GetInt32("currentNumber");
            }

            nextParcelId = "STU" + (currentAutoNumber + 1);
            reader.Close();
            DbConnection.StopDbConnection();

            return nextParcelId;
        }

        public static void UpdateNextParcelIdAutoNumber()
        {
            string query = "UPDATE id_autonumber SET currentNumber = (currentNumber + 1) WHERE type = 'parcel';";

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.ExecuteNonQuery();
        }

        public static void UpdateNextUserIdAutoNumber()
        {
            string query = "UPDATE id_autonumber SET currentNumber = (currentNumber + 1) WHERE type = 'user';";

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.ExecuteNonQuery();
        }

        public static void UpdateNextStudentIdAutoNumber()
        {
            string query = "UPDATE id_autonumber SET currentNumber = (currentNumber + 1) WHERE type = 'student';";

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.ExecuteNonQuery();
        }
    }
}
