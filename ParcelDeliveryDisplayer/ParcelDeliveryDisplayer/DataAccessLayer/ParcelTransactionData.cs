﻿using MySql.Data.MySqlClient;
using ParcelDeliveryDisplayer.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelDeliveryDisplayer.DataAccessLayer
{
    public class ParcelTransactionData
    {
        public ParcelTransactionData()
        {
            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }
        }

        public void InsertParcelTransaction(Parcel parcel, Student student, User user)
        {
            string query = "INSERT INTO parcel_transaction(parcelId, studentId, receiverId, parcelStatus, dateReceived)" +
               " VALUES (?parcelId, ?studentId, ?receiverId, ?parcelStatus, ?dateReceived)";
            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.Parameters.AddWithValue("?parcelId", parcel.ParcelId);
            comm.Parameters.AddWithValue("?studentId", student.studentId);
            comm.Parameters.AddWithValue("?receiverId", user.UserId);
            comm.Parameters.AddWithValue("?parcelStatus", "New");
            comm.Parameters.AddWithValue("?dateReceived", DateTime.Now);

            comm.ExecuteNonQuery();

        }

        public void UpdateParcelTransaction(ParcelTransaction trans)
        {
            string query = "UPDATE parcel_transaction SET parcelStatus = @parcelStatus, dateCollected = @dateCollected, dateReturned = @dateReturned" +
              " WHERE id = @id";
            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.Parameters.AddWithValue("@id", trans.Id);
            comm.Parameters.AddWithValue("@parcelStatus", trans.ParcelStatus);
            if(trans.ParcelStatus == "Collected")
            {
                comm.Parameters.AddWithValue("@dateReturned", null);
                comm.Parameters.AddWithValue("@dateCollected", DateTime.Now);

            }
            else if(trans.ParcelStatus == "Returned")
            {
                comm.Parameters.AddWithValue("@dateReturned", DateTime.Now);
                comm.Parameters.AddWithValue("@dateCollected", null);
            }
            else
            {
                comm.Parameters.AddWithValue("@dateReturned", null);
                comm.Parameters.AddWithValue("@dateCollected", null);
            }

            comm.ExecuteNonQuery();
        }
        public List<ParcelTransaction> GetAllParcelTransactions()
        {
            List<ParcelTransaction> parcelTrans = new List<ParcelTransaction>();
            DataTable dataTable = new DataTable();

            //join table parcel,student,user and parcel_transaction
            string query = "SELECT PT.id,PAR.parcelId ,PAR.barcode, PT.dateReceived, USR.name as receivedBy, PT.studentId, STU.name as studentName," +
                "STU.matricNo as studentMatricNo, STU.hpNo, PT.parcelStatus, PT.dateCollected, PT.dateReturned from parcel_transaction PT" +
                " LEFT JOIN student STU on STU.studentId = PT.studentId " +
                " LEFT JOIN parcel PAR on PAR.parcelId = PT.parcelId " +
                "LEFT JOIN user USR on USR.userId = PT.receiverId; ";

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }
            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);

            using (MySqlDataAdapter adapter = new MySqlDataAdapter(comm))
            {
                adapter.Fill(dataTable);
            }

            foreach(DataRow row in dataTable.Rows)
            {
                ParcelTransaction parcelTran = new ParcelTransaction();
                parcelTran.Id = (int)row["id"];
                parcelTran.ParcelId = row["parcelId"].ToString();
                parcelTran.StudentId = row["studentId"].ToString();
                parcelTran.Barcode = row["barcode"].ToString();
                parcelTran.StudentName = row["studentName"].ToString();
                parcelTran.StudentMatricNo = row["studentMatricNo"].ToString();
                parcelTran.HpNo = row["hpNo"].ToString();
                parcelTran.ReceivedBy = row["receivedBy"].ToString();
                parcelTran.ParcelStatus = row["parcelStatus"].ToString();
                parcelTran.DateReceived = (DateTime)row["dateReceived"];

                if (!DBNull.Value.Equals(row["dateCollected"]))
                {
                    parcelTran.DateCollected = (DateTime)row["dateCollected"];
                }

                if (!DBNull.Value.Equals(row["dateCollected"]))
                {
                    parcelTran.DateCollected = (DateTime)row["dateReceived"];
                }

                parcelTrans.Add(parcelTran);
            }

            return parcelTrans;
        }

        public void DeleteParcelTransaction(int transId)
        {
            string query = "DELETE FROM parcel_transaction WHERE id = @id";
            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.Parameters.AddWithValue("@id", transId);

            comm.ExecuteNonQuery();
            DbConnection.CloseDbConnection();
        }
    }
}
