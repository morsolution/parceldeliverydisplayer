﻿using MySql.Data.MySqlClient;
using ParcelDeliveryDisplayer.Objects;
using System;
using System.Collections.Generic;

namespace ParcelDeliveryDisplayer.DataAccessLayer
{
    public class StudentData
    {
        public StudentData()
        {
            
        }

        public void InsertStudent(Student student)
        {
            string query = "INSERT INTO student(name, studentId, matricNo, hpNo)" +
               " VALUES (?name, ?studentId, ?matricNo, ?hpNo)";

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.Parameters.AddWithValue("?name", student.Name);
            comm.Parameters.AddWithValue("?studentId", IdAutoNumberData.GetNextStudentId());
            comm.Parameters.AddWithValue("?matricNo", student.MatricNo);
            comm.Parameters.AddWithValue("?hpNo", student.HpNo);

            comm.ExecuteNonQuery();

        }

        public void UpdateStudent(Student student)
        {
            string query = "UPDATE student SET name = @name, matricNo = @matricNo, hpNo= @hpNo" +
               " WHERE studentId = @studentId";
            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.Parameters.AddWithValue("@studentId", student.studentId);
            comm.Parameters.AddWithValue("@name", student.Name);
            comm.Parameters.AddWithValue("@matricNo", student.MatricNo);
            comm.Parameters.AddWithValue("@hpNo", student.HpNo);

            comm.ExecuteNonQuery();
            DbConnection.CloseDbConnection();

        }

        public Student GetStudentByStudentId(string studentId)
        {
            Student student = new Student();

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand command = DbConnection.Connection.CreateCommand();
            command.CommandText = "SELECT * FROM student where studentId = @studentId";
            command.Parameters.AddWithValue("@studentId", studentId);

            try
            {
                MySqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    student.Id = reader.GetInt32("id");
                    student.studentId = reader.GetString("studentId");
                    student.Name = reader.GetString("name");
                    student.MatricNo= reader.GetString("matricNo");
                    student.HpNo= reader.GetString("hpNo");
                }

                reader.Close();
            }
            catch (Exception ex)
            {

            }

            DbConnection.CloseDbConnection();

            return student;
        }

        public Student GetStudentByName()
        {
            return new Student();
        }

        public Student GetStudentByHpNo()
        {
            return new Student();
        }
        public Student GetStudentByMatricNo()
        {
            return new Student();
        }
        private List<Student> GetStudents()
        {
            return new List<Student>();
        }
    }
}
