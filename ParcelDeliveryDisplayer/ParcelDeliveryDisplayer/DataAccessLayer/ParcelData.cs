﻿using MySql.Data.MySqlClient;
using ParcelDeliveryDisplayer.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelDeliveryDisplayer.DataAccessLayer
{
    public class ParcelData
    {
        public ParcelData()
        {

        }
        public void InsertParcel(Parcel parcel)
        {
            string query = "INSERT INTO parcel(parcelId, barcode, dateReceived)" +
                " VALUES (?parcelId, ?barcode, ?dateReceived)";
            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.Parameters.AddWithValue("?parcelId", IdAutoNumberData.GetNextParcelId());
            comm.Parameters.AddWithValue("?barcode", parcel.Barcode);
            comm.Parameters.AddWithValue("?dateReceived", parcel.DateReceived);

            comm.ExecuteNonQuery();

        }

        public void UpdateParcel(Parcel parcel)
        {
            string query = "UPDATE parcel SET barcode = @barcode" +
               " WHERE parcelId = @parcelId";
            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand comm = new MySqlCommand(query, DbConnection.Connection);
            comm.Parameters.AddWithValue("@parcelId", parcel.ParcelId);
            comm.Parameters.AddWithValue("@barcode", parcel.Barcode);

            comm.ExecuteNonQuery();
            DbConnection.CloseDbConnection();

        }

        public Parcel GetParcelByBarcode(string barcode)
        {
            Parcel parcel = new Parcel();

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand command = DbConnection.Connection.CreateCommand();
            command.CommandText = "SELECT * FROM parcel where barcode = @barcode";
            command.Parameters.AddWithValue("@barcode", barcode);

            try
            {
                MySqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    parcel.Id = reader.GetInt32("id");
                    parcel.Barcode = reader.GetString("barcode");
                    parcel.DateReceived = reader.GetDateTime("dateReceived");
                    parcel.ParcelId = reader.GetString("parcelId");
                }

                reader.Close();
            }
            catch (Exception ex)
            {

            }
            DbConnection.CloseDbConnection();

            return parcel;
        }

        public Parcel GetParcelByParcelId(string parcelId)
        {
            Parcel parcel = new Parcel();

            if (DbConnection.Connection == null)
            {
                DbConnection.StartDbConnection();
            }

            if (DbConnection.Connection.State == System.Data.ConnectionState.Closed)
            {
                DbConnection.Connection.Open();
            }

            MySqlCommand command = DbConnection.Connection.CreateCommand();
            command.CommandText = "SELECT * FROM parcel where parcelId = @parcelId";
            command.Parameters.AddWithValue("@parcelId", parcelId);

            try
            {
                MySqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    parcel.Id = reader.GetInt32("id");
                    parcel.Barcode = reader.GetString("barcode");
                    parcel.DateReceived = reader.GetDateTime("dateReceived");
                    parcel.ParcelId = reader.GetString("parcelId");
                }

                reader.Close();
            }
            catch (Exception ex)
            {

            }

            DbConnection.CloseDbConnection();
            return parcel;
        }
    }
}
