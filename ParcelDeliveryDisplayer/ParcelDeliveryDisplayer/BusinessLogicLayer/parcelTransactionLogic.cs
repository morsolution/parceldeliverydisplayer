﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParcelDeliveryDisplayer.Objects;
using ParcelDeliveryDisplayer.DataAccessLayer;

namespace ParcelDeliveryDisplayer.BusinessLogicLayer
{
    public class parcelTransactionLogic
    {
        private ParcelTransactionData parcelTransactionData;

        public parcelTransactionLogic()
        {
            this.parcelTransactionData = new ParcelTransactionData();
        }
        public List<ParcelTransaction> GetParcelTransactionDataSource()
        {
            return this.parcelTransactionData.GetAllParcelTransactions();
        }

        public List<ParcelTransaction> GetParcelTransactionDataSourceForMonitor()
        {
            List<ParcelTransaction> allTrans = this.GetParcelTransactionDataSource();
            List<ParcelTransaction> result = new List<ParcelTransaction>();

            foreach(ParcelTransaction trans in allTrans)
            {
                if(trans.ParcelStatus == "New")
                {
                    result.Add(trans);
                }
            }

            return result;
        }
        public List<ParcelTransaction> GetAllParcelTransactionsByFilter(string filterType, string filter)
        {
            List<ParcelTransaction> filterResult = null;
            List<ParcelTransaction> allTrans = this.parcelTransactionData.GetAllParcelTransactions();

            if (filterType == "Barcode")
            {
                filterResult = allTrans.Where(x => x.Barcode.StartsWith(filter)).ToList();
            }

            if (filterType == "Name")
            {
                filterResult = allTrans.Where(x => x.StudentName.StartsWith(filter)).ToList();
            }

            if (filterType == "HpNo")
            {
                filterResult = allTrans.Where(x => x.HpNo.StartsWith(filter)).ToList();

            }


            return filterResult;
        }
        public bool DeleteParcelTransaction(int id)
        {
            try
            {
                this.parcelTransactionData.DeleteParcelTransaction(id);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}
