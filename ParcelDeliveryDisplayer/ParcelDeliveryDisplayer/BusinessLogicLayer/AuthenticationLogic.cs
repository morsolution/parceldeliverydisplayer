﻿using ParcelDeliveryDisplayer.DataAccessLayer;
using ParcelDeliveryDisplayer.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelDeliveryDisplayer.BusinessLogicLayer
{
    public class AuthenticationLogic
    {
        private UserData usersDataClass;
        public AuthenticationLogic()
        {
            this.usersDataClass = new UserData();
        }
        public bool ValidateUser(User user)
        {
            bool result = false;
            User userFromDatabase = this.usersDataClass.RetrieveUserByUsername(user.Username);
            if(userFromDatabase.Password == user.Password)
            {
                result = true;
                //set user session for entire application use
                UserSession.SetUserSession(userFromDatabase);
            }
            return result;
        }
    }
}
