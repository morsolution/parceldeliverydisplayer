﻿using ParcelDeliveryDisplayer.Objects;
using ParcelDeliveryDisplayer.DataAccessLayer;
using System;

namespace ParcelDeliveryDisplayer.BusinessLogicLayer
{
    public class ParcelLogic
    {
        StudentData studentDataLayer;
        ParcelData parcelDataLayer;
        ParcelTransactionData parcelTransDataLayer;
        public ParcelLogic()
        {
            this.studentDataLayer = new StudentData();
            this.parcelDataLayer = new ParcelData();
            this.parcelTransDataLayer = new ParcelTransactionData();
        }

        public bool AddParcel(Parcel parcel, Student student)
        {
            bool result = true;
            try
            {
                //insert into parcel
                parcel.ParcelId = IdAutoNumberData.GetNextParcelId();
                this.parcelDataLayer.InsertParcel(parcel);
                IdAutoNumberData.UpdateNextParcelIdAutoNumber();

                //insert into student
                student.studentId = IdAutoNumberData.GetNextStudentId();
                this.studentDataLayer.InsertStudent(student);
                IdAutoNumberData.UpdateNextStudentIdAutoNumber();
                //insert into parcelTrans
                this.parcelTransDataLayer.InsertParcelTransaction(parcel, student, UserSession.getCurrentUser());
            }
            catch (Exception ex)
            {
                return false;
            }
            
            return result;
        }

        public bool UpdateParcel (Parcel parcel, Student student, ParcelTransaction trans)
        {
            try
            {
                //update parcel
                this.parcelDataLayer.UpdateParcel(parcel);

                //update student

                this.studentDataLayer.UpdateStudent(student);

                //update parcel transaction
                this.parcelTransDataLayer.UpdateParcelTransaction(trans);
            }
            catch (Exception ex)
            {
                return false;
            }
            

            return true;
        }

        public bool UpdateParcelCollection(Student student, ParcelTransaction trans)
        {
            try
            {
                this.studentDataLayer.UpdateStudent(student);
                this.parcelTransDataLayer.UpdateParcelTransaction(trans);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public Parcel GetParcelByParcelId(string parcelId)
        {
            Parcel parcel = this.parcelDataLayer.GetParcelByParcelId(parcelId);
            return parcel;
        }
        public bool VerifyParcelExists(string barcode)
        {
            bool result = true;

            Parcel parc = this.parcelDataLayer.GetParcelByBarcode(barcode);
            if(parc != null)
            {
                result = false;
            }

            return result;
        }
    }
}
