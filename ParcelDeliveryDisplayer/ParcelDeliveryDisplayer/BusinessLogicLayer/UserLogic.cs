﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParcelDeliveryDisplayer.Objects;
using ParcelDeliveryDisplayer.DataAccessLayer;

namespace ParcelDeliveryDisplayer.BusinessLogicLayer
{
    public class UserLogic
    {
        private UserData userDataLayer;
        public UserLogic()
        {
            this.userDataLayer = new UserData();

        }

        public User GetCurrentUser(string username)
        {
            User user = this.userDataLayer.RetrieveUserByUsername(username);
            return user;
        }

        public bool DeleteUserByUserId(string userId)
        {
            try
            {
                this.userDataLayer.DeleteUserByUserId(userId);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        public List<User> GetAllUsers()
        {
            return this.userDataLayer.RetrieveAllUsers();
        }
    }
}
