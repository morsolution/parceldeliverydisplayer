﻿using System;
using ParcelDeliveryDisplayer.DataAccessLayer;
using ParcelDeliveryDisplayer.Objects;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelDeliveryDisplayer.BusinessLogicLayer
{
    public class StudentLogic
    {
        private Student student;
        private StudentData studentDataLayer;
        public StudentLogic()
        {
            this.studentDataLayer = new StudentData();
        }

        public Student GetStudentByStudentId(string studentId)
        {
            Student student = this.studentDataLayer.GetStudentByStudentId(studentId);
            return student;
        }


    }
}
