﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelDeliveryDisplayer.Objects
{
    public class Student
    {
        public Student()
        {

        }
        public int Id { get; set; }
        public string studentId { get; set; }
        public string Name { get; set; }
        public string MatricNo { get; set; }
        public string HpNo { get; set; }
    }
}
