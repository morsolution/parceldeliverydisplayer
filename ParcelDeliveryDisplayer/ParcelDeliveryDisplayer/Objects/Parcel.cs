﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelDeliveryDisplayer.Objects
{
    public class Parcel
    {
        public int Id { get; set; }
        public string ParcelId { get; set; }
        public string Barcode { get; set; }
        public DateTime DateReceived { get; set; }

    }
}
