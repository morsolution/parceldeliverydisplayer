﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParcelDeliveryDisplayer.Objects;

namespace ParcelDeliveryDisplayer.Objects
{
    public class LoginTransactions
    {

        public int UserId { get; set; }
        public DateTime LoginDate { get; set; }
        public TimeSpan LoginTime { get; set; }
        public TimeSpan LogoutTime { get; set; }
        //public int Id { get; set; }
    }
}
