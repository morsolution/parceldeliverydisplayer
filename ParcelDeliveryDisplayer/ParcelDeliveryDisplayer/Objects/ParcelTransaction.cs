﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelDeliveryDisplayer.Objects
{
    public class ParcelTransaction
    {
        public ParcelTransaction() { }
        public int Id { get; set; }
        public string ParcelId { get; set; }
        public string StudentId { get; set; }
        public string UserId { get; set; }

        public string ReceivedBy { get; set; }
        public DateTime DateReceived { get; set; }
        public DateTime DateCollected { get; set; }
        public DateTime DateReturned { get; set; }
        public string StudentName { get; set; }
        public string StudentMatricNo { get; set; }
        public string HpNo { get; set; }
        public string Barcode { get; set; }
        public string ParcelStatus { get; set; }



    }
}
