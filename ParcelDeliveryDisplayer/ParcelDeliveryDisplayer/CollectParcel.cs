﻿using System;
using ParcelDeliveryDisplayer.BusinessLogicLayer;
using ParcelDeliveryDisplayer.Objects;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParcelDeliveryDisplayer
{
    public partial class CollectParcel : Form
    {
        private ParcelTransaction parcelTrans;
        private ParcelLogic parcelLogicLayer;
        private StudentLogic studentLogicLayer;

        private Student student;
        public CollectParcel(ParcelTransaction trans)
        {
            this.parcelTrans = trans;
            this.parcelLogicLayer = new ParcelLogic();
            this.studentLogicLayer = new StudentLogic();

            this.student = this.studentLogicLayer.GetStudentByStudentId(this.parcelTrans.StudentId);

            InitializeComponent();
            PopulateFields();
            ToogleButtonsEnability();
        }

        private void ToogleButtonsEnability()
        {
            this.textBoxBarcode.Enabled = false;
            this.textBoxStudentName.Enabled = false;
            this.textBoxHpNo.Enabled = false;
        }

        private void PopulateFields()
        {
            this.textBoxParcelId.Text = this.parcelTrans.ParcelId;
            this.textBoxBarcode.Text = this.parcelTrans.Barcode;
            this.textBoxStudentName.Text = this.parcelTrans.StudentName;
            this.textBoxStudentMatricNo.Text = this.parcelTrans.StudentMatricNo;
            this.textBoxHpNo.Text = this.parcelTrans.HpNo;
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if(ValidateFields() == false)
            {
                return;
            }

            this.student.MatricNo = this.textBoxStudentMatricNo.Text;
            this.parcelTrans.ParcelStatus = "Collected";

            if(this.parcelLogicLayer.UpdateParcelCollection(this.student, this.parcelTrans) == true)
            {
                MessageBox.Show("Thank you! Please collect your parcel from the counter!");
                this.Close();
            }

        }

        private bool ValidateFields()
        {
            bool result = true;

            if(String.IsNullOrEmpty(this.textBoxStudentMatricNo.Text))
            {
                result = false;
                MessageBox.Show("Matric No is required for collection!");
            }

            return result;
        }
    }
}
