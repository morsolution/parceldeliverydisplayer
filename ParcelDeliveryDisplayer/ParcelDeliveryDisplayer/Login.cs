﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using ParcelDeliveryDisplayer.Objects;
using ParcelDeliveryDisplayer.BusinessLogicLayer;
using ParcelDeliveryDisplayer.DataAccessLayer;
using System.Windows.Forms;

namespace ParcelDeliveryDisplayer
{
    public partial class Login : Form
    {
        private User user;
        private AuthenticationLogic authenticationClass;

        //constructor
        public Login()
        {
            InitializeComponent();

            this.user = new User();
            this.authenticationClass = new AuthenticationLogic();
        }

        //1st method that will be run automatically upon load
        private void Login_Load(object sender, EventArgs e)
        {
            //if no database create, ask to generate
            DbConnection.StartDbConnection(); //connect to db
            if(!DbConnection.HasDatabase)
            {

            }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            string username = this.textBoxUsername.Text;
            string password = this.textBoxPassword.Text;

            this.user.Username = username;
            this.user.Password = password;

            if (validateFields() == false)
            {
                MessageBox.Show("All fields are required!");
                return;
            }

            bool result = this.authenticationClass.ValidateUser(this.user);

            if (result)
            {
                Global.CurrentUserUsername = this.user.Username;
                Main mainForm = new Main();
                mainForm.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Incorrect username" + "/" + "password!");
            }
        }

        private bool validateFields()
        {
            bool result = true;
            if (string.IsNullOrEmpty(this.textBoxUsername.Text))
            {
                result = false;
            }

            if (string.IsNullOrEmpty(this.textBoxPassword.Text))
            {
                result = false;
            }
            return result;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            ClearTextboxes();
        }
        private void ClearTextboxes()
        {
            this.textBoxPassword.Text = string.Empty;
            this.textBoxUsername.Text = string.Empty;
            this.user.Username = string.Empty;
            this.user.Password = string.Empty;
        }
    }
}
